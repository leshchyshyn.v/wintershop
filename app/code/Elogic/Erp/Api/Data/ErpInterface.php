<?php

namespace Elogic\Erp\Api\Data;

interface ErpInterface
{
    const ENTITY_ID     = "entity_id";
    const ORDER_ID      = "order_id";
    const ERP_ID        = "erp_id";

    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getOrderId();

    /**
     * @return string
     */
    public function getErpId();

    /**
     * @param int $id
     * @return ErpInterface
     */
    public function setId($id);

    /**
     * @param int $orderId
     * @return ErpInterface
     */
    public function setOrderId($orderId);

    /**
     * @param string $erpId
     * @return ErpInterface
     */
    public function setErpId($erpId);
}