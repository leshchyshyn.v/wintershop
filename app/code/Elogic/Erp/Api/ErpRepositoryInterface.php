<?php

namespace Elogic\Erp\Api;

interface ErpRepositoryInterface
{
    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return \Elogic\Erp\Api\Data\ErpInterface
     */
    public function getById($id);

    /**
     * @param \Elogic\Erp\Api\Data\ErpInterface $erp
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return bool
     */
    public function delete($erp);

    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return bool
     */
    public function deleteById($id);

    /**
     * @param \Elogic\Erp\Api\Data\ErpInterface $erp
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return \Elogic\Erp\Api\Data\ErpInterface
     */
    public function save($erp);

    /**
     * @param $orderId
     * @return ErpInterface Erp
     */
    public function getErpByOrderId($orderId);
}