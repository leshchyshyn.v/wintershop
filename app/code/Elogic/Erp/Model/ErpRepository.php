<?php

namespace Elogic\Erp\Model;

use Elogic\Erp\Api\Data\ErpInterface;
use Elogic\Erp\Api\ErpRepositoryInterface;
use Elogic\Erp\Model\ResourceModel\Erp as ErpResource;
use Elogic\Erp\Model\ErpFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class ErpRepository implements ErpRepositoryInterface
{
    /**
     * @var ErpResource
     */
    private $erpResource;
    /**
     * @var \Elogic\Erp\Model\ErpFactory
     */
    private $erpFactory;

    /**
     * ErpRepository constructor.
     * @param ErpResource $erpResource
     * @param \Elogic\Erp\Model\ErpFactory $erpFactory
     */
    public function __construct(
        ErpResource $erpResource,
        ErpFactory $erpFactory
    )
    {

        $this->erpResource = $erpResource;
        $this->erpFactory = $erpFactory;
    }

    public function getById($id)
    {
        $erp = $this->erpFactory->create();
        $this->erpResource->load($erp, $id);
        if (!$erp->getId()) {
            throw new NoSuchEntityException(__('The erp entrywith the "%1" ID doesn\'t exist.', $id));
        }
        return $erp;
    }

    public function save($erp)
    {
        try {
            $this->erpResource->save($erp);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $erp;
    }

    public function delete($erp)
    {
        try {
            $this->erpResource->delete($erp);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @param $orderId
     * @return ErpInterface Erp
     */
    public function getErpByOrderId($orderId)
    {
        $erp = $this->erpFactory->create();
        $this->erpResource->load($erp, $orderId, ErpInterface::ORDER_ID);
        return $erp;
    }

}