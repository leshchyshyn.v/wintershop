<?php

namespace Elogic\Erp\Model;

use Elogic\Erp\Api\Data\ErpInterface;
use Magento\Framework\Model\AbstractModel;

class Erp extends AbstractModel implements ErpInterface
{
    public function _construct()
    {
        $this->_init(\Elogic\Erp\Model\ResourceModel\Erp::class);
    }

    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    public function getErpId()
    {
        return $this->getData(self::ERP_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    public function setErpId($erpId)
    {
        return $this->setData(self::ERP_ID, $erpId);
    }
}