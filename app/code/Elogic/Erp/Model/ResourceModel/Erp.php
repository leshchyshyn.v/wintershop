<?php

namespace Elogic\Erp\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Erp extends AbstractDb
{
    public function _construct()
    {
        $this->_init('elogic_erp', 'entity_id');
    }
}