<?php

namespace Elogic\Erp\Plugin;

use Elogic\Erp\Api\ErpRepositoryInterface;
use Magento\Sales\Api\Data\OrderExtensionFactory;

class OrderGet
{
    /**
     * @var ErpRepositoryInterface
     */
    private $erpRepository;
    /**
     * @var OrderExtensionFactory
     */
    private $orderExtensionFactory;

    /**
     * OrderGet constructor.
     * @param ErpRepositoryInterface $erpRepository
     * @param OrderExtensionFactory $orderExtensionFactory
     */
    public function __construct(
        ErpRepositoryInterface $erpRepository,
        OrderExtensionFactory $orderExtensionFactory
    )
    {
        $this->erpRepository = $erpRepository;
        $this->orderExtensionFactory = $orderExtensionFactory;
    }

    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    )
    {
        $resultOrder = $this->getErpAttribute($resultOrder);

        return $resultOrder;
    }

    private function getErpAttribute(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $erp = $this->erpRepository->getErpByOrderId($order->getEntityId());
        if (!$erp->getId()) {
            return $order;
        }

        $extensionAttributes = $order->getExtensionAttributes();
        $orderExtension = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();

        $orderExtension->setErpAttribute($erp);


//        $foomanAttribute = $this->foomanAttributeFactory->create();
//        $foomanAttribute->setValue($foomanAttributeValue);
//        $orderExtension->setFoomanAttribute($foomanAttribute);
        $order->setExtensionAttributes($orderExtension);

        return $order;
    }
}