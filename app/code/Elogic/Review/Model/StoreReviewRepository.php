<?php

namespace Elogic\Review\Model;

use Elogic\Review\Api\StoreReviewRepositoryInterface;
use Elogic\Review\Api\Data;
use Elogic\Review\Model\ResourceModel\StoreReview as ResourceStoreReview;
use Magento\Framework\Exception\CouldNotSaveException;

class StoreReviewRepository implements StoreReviewRepositoryInterface
{
    /**
     * @var ResourceStoreReview
     */
    protected $resource;

    /**
     * @var StoreReviewFactory
     */
    protected $storeReviewFactory;

    /**
     * StoreReviewRepository constructor.
     * @param ResourceStoreReview $resourceStoreReview
     * @param StoreReviewFactory $storeReviewFactory
     */
    public function __construct(
        ResourceStoreReview $resourceStoreReview,
        StoreReviewFactory $storeReviewFactory
    )
    {
        $this->resource = $resourceStoreReview;
        $this->storeReviewFactory = $storeReviewFactory;
    }

    /**
     * Save store review.
     *
     * @param \Elogic\Review\Api\Data\StoreReviewInterface $storeReview
     * @return \Elogic\Review\Api\Data\StoreReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\StoreReviewInterface $storeReview)
    {
        try {
            /** @var $storeReview \Elogic\Review\Model\StoreReview */
            $this->resource->save($storeReview);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $storeReview;
    }

    /**
     * Retrieve store review.
     *
     * @param int $storeReviewId
     * @return \Elogic\Review\Api\Data\StoreReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeReviewId)
    {
        $storeReview = $this->storeReviewFactory->create();
        $this->resource->load($storeReview, $storeReviewId);
        if (!$storeReview->getId()) {
            throw new NoSuchEntityException(__('The CMS block with the "%1" ID doesn\'t exist.', $storeReviewId));
        }
        return $storeReview;
    }

    /**
     * Delete store review.
     *
     * @param \Elogic\Review\Api\Data\StoreReviewInterface $storeReview
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\StoreReviewInterface $storeReview)
    {
        try {
            /** @var $storeReview \Elogic\Review\Model\StoreReview */
            $this->resource->delete($storeReview);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete store review by ID.
     *
     * @param int $storeReviewId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storeReviewId)
    {
        return $this->delete($this->getById($storeReviewId));
    }
}