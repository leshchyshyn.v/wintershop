<?php

namespace Elogic\Review\Model\ResourceModel\StoreReview;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'review_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Elogic\Review\Model\StoreReview::class, \Elogic\Review\Model\ResourceModel\StoreReview::class);
    }
}