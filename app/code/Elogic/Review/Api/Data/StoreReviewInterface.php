<?php

namespace Elogic\Review\Api\Data;

/**
 * Interface StoreReviewInterface
 * @package Elogic\Review\Api\Data
 */
interface StoreReviewInterface
{
    const REVIEW_ID     = "review_id";
    const CUSTOMER_NAME = "customer_name";
    const CONTENT       = "content";
    const CREATION_TIME = "creation_time";
    const UPDATE_TIME   = "update_time";
    const IS_ACTIVE     = "is_active";

    /**
     * Get ID
     *
     * @return int|null;
     */
    public function getId();

    /**
     * Get customer name
     *
     * @return string|null
     */
    public function getCustomerName();

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * is Active
     *
     * @return int|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return StoreReviewInterface
     */
    public function setId($id);

    /**
     * Set customer id
     *
     * @param string $customerName
     * @return StoreReviewInterface
     */
    public function setCustomerName($customerName);

    /**
     * Set content
     *
     * @param string $content
     * @return StoreReviewInterface
     */
    public function setContent($content);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return StoreReviewInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return StoreReviewInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int $isActive
     * @return StoreReviewInterface
     */
    public function setIsActive($isActive);
}
