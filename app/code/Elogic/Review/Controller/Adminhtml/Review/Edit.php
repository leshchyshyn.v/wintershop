<?php

namespace Elogic\Review\Controller\Adminhtml\Review;

use Elogic\Review\Api\StoreReviewRepositoryInterface;
use Elogic\Review\Model\StoreReviewFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;

/**
 * Edit CMS block action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var StoreReviewRepositoryInterface
     */
    private $storeReviewRepository;
    /**
     * @var StoreReviewFactory
     */
    private $storeReviewFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param StoreReviewRepositoryInterface $storeReviewRepository
     * @param StoreReviewFactory $storeReviewFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        StoreReviewRepositoryInterface $storeReviewRepository,
        StoreReviewFactory $storeReviewFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->storeReviewRepository = $storeReviewRepository;
        $this->storeReviewFactory = $storeReviewFactory;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Edit review
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('review_id');


        // 2. Initial checking
        if ($id) {
            $model = $this->storeReviewRepository->getById($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This review no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $model = $this->storeReviewFactory->create();
        }

        $this->coreRegistry->register('store_review', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Elogic_Review::review')
            ->addBreadcrumb(__('Store Review'), __('Store Review'));
        $resultPage->getConfig()->getTitle()->prepend(__('Reviews'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getId() : __('New Review'));
        return $resultPage;
    }
}
