<?php

namespace Elogic\Review\Controller\Adminhtml\Review;

use Elogic\Review\Api\StoreReviewRepositoryInterface;
use Magento\Backend\App\Action;

class Delete extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Elogic_Review::edit';

    /**
     * @var StoreReviewRepositoryInterface
     */
    private $storeReviewRepository;

    /**
     * Delete constructor.
     * @param StoreReviewRepositoryInterface $storeReviewRepository
     * @param Action\Context $context
     */
    public function __construct(
        StoreReviewRepositoryInterface $storeReviewRepository,
        Action\Context $context
    )
    {
        parent::__construct($context);
        $this->storeReviewRepository = $storeReviewRepository;
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('review_id');
        if ($id) {
            try {
                $this->storeReviewRepository->deleteById($id);
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the review.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/');
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a review to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}