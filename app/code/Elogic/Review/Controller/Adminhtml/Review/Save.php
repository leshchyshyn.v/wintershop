<?php

namespace Elogic\Review\Controller\Adminhtml\Review;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action\Context;
use Elogic\Review\Api\StoreReviewRepositoryInterface;
use Elogic\Review\Model\StoreReview;
use Elogic\Review\Model\StoreReviewFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Save store review action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var StoreReviewFactory
     */
    private $storeReviewFactory;

    /**
     * @var StoreReviewRepositoryInterface
     */
    private $storeReviewRepository;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param StoreReviewFactory|null $storeReviewFactory
     * @param StoreReviewRepositoryInterface|null $storeReviewRepository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        StoreReviewFactory $storeReviewFactory = null,
        StoreReviewRepositoryInterface $storeReviewRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->storeReviewFactory = $storeReviewFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StoreReviewFactory::class);
        $this->storeReviewRepository = $storeReviewRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StoreReviewRepositoryInterface::class);
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = StoreReview::STATUS_ENABLED;
            }
            if (empty($data['review_id'])) {
                $data['review_id'] = null;
            }

            /** @var \Elogic\Review\Model\StoreReview $model */

            $id = $this->getRequest()->getParam('review_id');
            if ($id) {
                try {
                    $model = $this->storeReviewRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This review no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } else {
                $model = $this->storeReviewFactory->create();
            }

            $model->setData($data);

            try {
                $model = $this->storeReviewRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved reveiw.'));
                $this->dataPersistor->clear('store_review');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the block.'));
            }

            $this->dataPersistor->set('store_review', $data);
            return $resultRedirect->setPath('*/*/edit', ['review_id' => $model->getId()]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
