<?php

namespace Elogic\Firsttask\Plugin;

class Page
{

    public function afterGetTitle(\Magento\Cms\Model\Page $subject, $result)
    {
        $result = $result." Elogic";

        return $result;
    }

    public function beforeSetTitle(\Magento\Cms\Model\Page $subject, $args)
    {
        var_dump($args);
        return $args;
    }

    public function aroundGetTitle(\Magento\Cms\Model\Page $subject, callable $proceed)
    {
        return $proceed();
    }
}
