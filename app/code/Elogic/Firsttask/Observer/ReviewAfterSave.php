<?php

namespace Elogic\Firsttask\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ReviewAfterSave implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $review = $observer->getEvent()->getObject();

        $fp = fopen(BP."/var/".$review->getId().".csv", 'w');

        fputcsv($fp, $review->getData());

        fclose($fp);

    }
}