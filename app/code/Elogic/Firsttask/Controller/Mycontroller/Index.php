<?php

namespace Elogic\Firsttask\Controller\Mycontroller;

use Elogic\Erp\Model\ErpRepository;
use Elogic\Erp\Model\ErpFactory;
use Elogic\Review\Api\StoreReviewRepositoryInterface;
use Elogic\Review\Model\StoreReview;
use Elogic\Review\Model\StoreReviewFactory;
use Elogic\Review\Api\Data\StoreReviewInterface;
use Magento\Cms\Model\PageRepository;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Index extends Action implements HttpGetActionInterface, HttpPostActionInterface
{

    protected $storeReviewFactory;
    protected $storeReviewRepository;
    protected $storeReview;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var ErpRepository
     */
    private $erpRepository;
    /**
     * @var ErpFactory
     */
    private $erpFactory;


    /**
     * Index constructor.
     * @param StoreReviewFactory $storeReviewFactory
     * @param StoreReviewRepositoryInterface $storeReviewRepository
     * @param StoreReviewInterface $storeReview
     * @param PageRepository $pageRepository
     * @param ErpRepository $erpRepository
     * @param ErpFactory $erpFactory
     * @param Context $context
     */
    public function __construct(
        StoreReviewFactory $storeReviewFactory,
        StoreReviewRepositoryInterface $storeReviewRepository,
        StoreReviewInterface $storeReview,
        PageRepository $pageRepository,
        ErpRepository $erpRepository,
        ErpFactory $erpFactory,
        Context $context
    )
    {
        $this->storeReviewFactory = $storeReviewFactory;
        $this->storeReviewRepository = $storeReviewRepository;
        $this->storeReview = $storeReview;
        parent::__construct($context);
        $this->pageRepository = $pageRepository;
        $this->erpRepository = $erpRepository;
        $this->erpFactory = $erpFactory;
    }


    public function execute()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $order = $objectManager->create('Magento\Sales\Model\Order')->load(1);

//        $order = $this->orderRepository->getById(1);

        $erp = $this->erpFactory->create();
        $erp->setErpId("2dfsfdsfsdf342");

        $order->getExtensionAttributes()->serErpAttribute($erp);
        $this->orderRepository->save($order);
        die;
//
//        $this->erpRepository->save($erp);
//
//        echo $erp->getId();
//        die;


//        $page = $this->pageRepository->getById(1);
//        echo $page->etData('title');
//        echo "<br/>";
//        $page->setTitle("New Title");die;


//        for($i =20; $i<30; $i++ )
//        {
//            $review = $this->storeReviewFactory->create();
//            $review->setCustomerName("observer")->setContent("observer");
//            $review->setCustomerName("my customer ".$i )->setContent("review ".$i);
//            $this->storeReviewRepository->save($review);
//            unset($review);
//        }


//        $review = $this->storeReviewRepository->getById(2);
//
//        echo $review->getContent();
//

//        echo $review->getId();

//        $review = $this->storeReviewFactory->create();
//        $review = $this->storeReviewRepository->getById(24);
//        $review->setIsActive(1);
//        $review = $this->storeReviewRepository->save($review);


        /** @var \Elogic\Review\Model\StoreReview $storeReview */
//        $storeReview = $this->storeReview;
//        $collection = $storeReview->getCollection();
//

//        $collection->addFieldToSelect('customer_name');
//
//        $collection->addFieldToFilter(
//            ['review_id', 'customer_name'],
//            [['eq' => 8], ['eq' => 'my customer 20']]
//        );

//        $collection->addFieldToFilter('customer_name', ['eq' => 'my customer 20']);
//


//        $collection->setOrder('review_id', \Zend_Db_Select::SQL_ASC);
//
//        $collection->getSelect()->reset(\Zend_Db_Select::COLUMNS)->columns("review_id");

//        $collection->load();
//
//        foreach ($collection as $item)
//        {
//            echo get_class($item);
//            var_dump($item->getData());
//        }
//
//        echo $collection->getSelect()->__toString();


        die();


        /** @var StoreReview $review */
//        $review = $collection->getFirstItem();
//        $review->setReviewTitle("Cool store");
//        $review->setCustomerName("Awesome customer");
//        $data["review_title"] = "Cool store";
//        $review->setData($data);
//        $this->storeReviewRepository->save($review);
//        echo ($review->getData('review_id'));
    }
}